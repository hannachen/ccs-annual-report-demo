'use strict';
/*global jQuery, YT*/
var youtube = (function($) {

  var $document = $(document),
      $videos = $('.video'),
      activePlayer = null;

  function onPlayerReady(e) {
  }

  function onPlayerStateChange(e) {
    switch(e.data) {
      case YT.PlayerState.PLAYING:
        if (activePlayer && activePlayer !== e.target) {
          // Pause any active players
          activePlayer.pauseVideo();
        }
        activePlayer = e.target;
        trackPlayEvent(e.target.gaAction);
        break;
    }
  }

  function trackPlayEvent(action) {
    $document.trigger({
      type: 'video-event',
      action: action
    });
  }

  function onYouTubeIframeAPIReady() {

    // Detach from global
    window.onYouTubeIframeAPIReady = null;

    // Initialize videos
    $videos.each(function(i, v) {
      let videoId = v.getAttribute('data-videoId'),
          playerId = v.getAttribute('data-playerId'),
          gaAction = v.getAttribute('data-analytics-action'),
          player = new YT.Player(playerId, {
            width: '560',
            height: '315',
            videoId: videoId,
            playerVars: {
              'autoplay': 0,
              'controls': 0,
              'showinfo': 0,
              'rel': 0,
              'autohide': 1,
              'iv_load_policy': 3
            },
            events: {
              'onReady': onPlayerReady,
              'onStateChange': onPlayerStateChange
            }
          });
      player.gaAction = gaAction;
    });
  }

  function injectLibraryScript() {

    let tag = document.createElement('script'),
      firstScriptTag = document.getElementsByTagName('script')[0];

    tag.src = 'https://www.youtube.com/iframe_api';
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = function() {
      onYouTubeIframeAPIReady();
    };
  }

  function init() {
    injectLibraryScript();
  }

  return {
    init: function() {
      init();
    }
  };
})(jQuery);
