'use strict';
/*global jQuery*/
var analytics = (function($) {

  /**
   * Initialize GA event tracking
   */
  function init() {

    const $document = $(document),
          $trackable = $('[data-analytics="ga"]');

    $trackable.on('click', function(e) {
      let currentTarget = e.currentTarget,
          category = currentTarget.getAttribute('data-analytics-category'),
          action = currentTarget.getAttribute('data-analytics-action'),
          label = currentTarget.getAttribute('data-analytics-label') || '';
      $.ga.event(category, action, label);
    });

    // Timeline
    $document.one('explore.start', function(e) {
      $.ga.event('Timeline', 'Explore Start', '');
    });

    // Video messages
    $document.on('video-event', function(e) {
      $.ga.event('Video Message', e.action, '');
    });

    // Scroll Tracking
    $(window).on('activate.bs.scrollspy', function(e) {
      if (e.relatedTarget === undefined) {
        return false;
      }
      let floorName = e.relatedTarget.replace('#', '');
      var label = '',
          category = '';
      switch(floorName) {
        case 'timeline':
          category = 'S1';
          label = 'Timeline';
          break;
        case 'messages':
          category = 'S2';
          label = 'Video';
          break;
        case 'about':
          category = 'S3';
          label = 'About';
          break;
        case 'performance':
          category = 'S4';
          label = 'Performance';
          break;
        case 'highlights':
          category = 'S5';
          label = 'Highlights';
          break;
        case 'glossary':
          category = 'S6';
          label = 'Glossary';
          break;
      }
      if (label.length && category.length) {
        $.ga.event(category, label, '');
      }
    });

  }

  return {
    init: function() {
      init();
    }
  };
})(jQuery);
