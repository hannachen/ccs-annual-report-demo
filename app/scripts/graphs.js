'use strict';

var graphs = (function ($) {

  /*global Intersection, Path, Rectangle, utils, TweenMax, TweenLite, TimelineLite*/

  var isInitialized = false,
      $lineGraph = $('.svg-line-graph'),
      $barGraph = $('.svg-bar-graph'),

      // Height of the graph in pixels
      graphHeight = 50,

      // Top offset of graph
      graphOffset = 15,
      valueLabelOffset = 2, // Space between point and value label
      lineGraphYears = ['2011', '2012', '2013', '2014', '2015'];

  function setupLineGraph() {

    // Graph settings
    $lineGraph.each(function(i, svg) {

      let $svg = $(svg),
          $line = $svg.find('.line'),
          $values = [],
          values = [],
          $points = [];

      // Years label
      $.each(lineGraphYears, function(i, year) {
        let $value = $svg.find('.value-' + year);
        $values.push($value);
        values.push(parseFloat($value.text()));
        $points.push($svg.find('.point-' + year));
      });

      let min = values.min(),
          max = values.max(),
          percentMax = max - min,
          pointsData = [],
          pointsString = [],
          animation = new TimelineLite({repeat: 0});

      // Update point position
      $.each($points, function(i, $point) {

        let point = $point.get(0),
            pointBB = point.getBBox(),
            percentValue = ((values[i] - min) / percentMax) * 100,
            pixelPosition = (percentValue * graphHeight) / 100,
            xPosition = parseFloat($point.attr('x')),
            yPosition = graphHeight - pixelPosition + graphOffset;

        // Update element position
        $point.attr('y', yPosition - (pointBB.height / 2));

        // Handle svg transforms using TweenMax for IE
        // By storing the timeline object into the array of points and playing them
        // on inview event
        if (utils.isIe) {
          let pointTween = new TweenMax(point, 0.5, {scale: 0, transformOrigin: '50% 50%'});
          animation.add(pointTween);
        }

        let newPointX = xPosition + (pointBB.width / 2),
            newPoint = { x: newPointX, y: yPosition };
        pointsData.push(newPoint);
        pointsString.push(newPoint.x + ',' + newPoint.y);
      });

      // Update line positions
      $line.attr('points', pointsString.join(' '));

      // Create an invisible path from polyline's path to use for shapes hit test
      let virtualLine = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      virtualLine.setAttribute('d', 'M' + pointsString.join(' '));
      virtualLine.setAttribute('style', 'stroke-width:3; fill:none; stroke:black; visibility:hidden;');
      svg.appendChild(virtualLine);

      // Update value position
      $.each($values, function(i, $value) {
        let currentPoint = pointsData[i],
            label = $value.get(0),
            valueLabelBB = label.getBBox(),
            virtualBox = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

        // Update label position
        $value.attr('x', valueLabelBB.x - (valueLabelBB.width / (utils.isIe ? 4 : 2))); // Center value label
        $value.attr('y', currentPoint.y - (valueLabelBB.height / 2) - valueLabelOffset);

        // Create an invisible text box from text label used for shapes hit test
        let updatedvalueLabelBB = label.getBBox();
        virtualBox.setAttributeNS(null, 'x', updatedvalueLabelBB.x);
        virtualBox.setAttributeNS(null, 'y', updatedvalueLabelBB.y);
        virtualBox.setAttributeNS(null, 'width', updatedvalueLabelBB.width);
        virtualBox.setAttributeNS(null, 'height', updatedvalueLabelBB.height);
        virtualBox.setAttribute('style', 'stroke-width:3; fill:none; stroke:black; visibility:hidden;');
        svg.appendChild(virtualBox);

        // Check for overlap with line, move to the other side of the line when there's an overlap
        let hitTest = Intersection.intersectPathShape(new Path(virtualLine), new Rectangle(virtualBox));
        if (hitTest.status === 'Intersection') {
          $value.attr('y', currentPoint.y + valueLabelBB.height + valueLabelOffset);
        }
      });

      // Handle animation in IE
      if (utils.isIe) {

        // Attach TimelineMax to the svg object
        $svg.timeline = animation;

        // Attach in view event
        $svg.parent('.graph').off('inview').on('inview', function() {
          $svg.timeline.play();
        });
      }

    });

  }

  function setupBarGraph() {

    // Graph settings
    $barGraph.each(function(i, svg) {
      let $svg = $(svg),
          bar = $svg.find('.actual').get(0),
          actual = bar.getBBox(),
          target = $svg.find('.target').get(0).getBBox(),

          $textElements = $svg.find('tspan'),
          $targetLabel = $textElements.filter('.text-target-label'),
          $targetValue = $textElements.filter('.text-target-value'),
          $actualLabel = $textElements.filter('.text-actual-label'),
          $actualValue = $textElements.filter('.text-actual-value');

      // Reverse target and actual text labels when actual value is larger tha target to avoid overlap.
      if (actual.width >= target.x) {

        let targetLabelBox = $targetLabel.get(0).getComputedTextLength(),
            targetValueBox = $targetValue.get(0).getComputedTextLength(),
            labelOffset = actual.width === target.x ? 10 : 0;
        $targetLabel.attr('x', target.x - targetLabelBox - labelOffset);
        $targetValue.attr('x', target.x - targetValueBox - labelOffset);
        $actualLabel.attr('x', actual.width);
        $actualValue.attr('x', actual.width);
      } else {

        let actualLabelBox = $actualLabel.get(0).getComputedTextLength(),
            actualValueBox = $actualValue.get(0).getComputedTextLength();

        $targetLabel.attr('x', target.x);
        $targetValue.attr('x', target.x);
        $actualLabel.attr('x', actual.width - actualLabelBox);
        $actualValue.attr('x', actual.width - actualValueBox);
      }

      if (utils.isIe) {
        TweenMax.set(bar, {scaleX: 0, transformOrigin: 'left center'});
        $svg.parent('.graph').off('inview').on('inview', function(e) {
          if (e.inView) {
            TweenLite.to(bar, 1.5, {scaleX: 1});
          } else {
            TweenLite.to(bar, 0.5, {scaleX: 0});
          }
        });
      }
    });
  }

  /**
   * Graphs setup.
   */
  function setupGraphs() {
    setupLineGraph();
    setupBarGraph();
  }

  /**
   * Initialize graphs.
   */
  function init() {

    isInitialized = true;

    // Setup graphs
    setupGraphs();
  }

  /**
   * Uninitialize this module.
   */
  function deinit() {
    isInitialized = false;
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    },
    isInitialized: function() {
      return isInitialized;
    },
    setupGraphs: function() {
      setupGraphs();
    }
  };
})(jQuery);
