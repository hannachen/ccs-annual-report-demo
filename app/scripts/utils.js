'use strict';

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};


var utils = (function ($, window) {
  return {

    /**
     * Checks if elements are in view.
     * @param {jQuery} $element - jQuery element to check
     * @param {jQuery} $window - jQuery selected window element
     * @returns {boolean} inView - in view status
     */
    isElementsInView: function($element, $window) {
      var docViewTop = $window.scrollTop(),
          docViewBottom = docViewTop + $window.height(),
          inView = false;

      let $el = $element,
          elTop = $el.offset().top,
          elBottom = elTop + $el.height();

      if ((elBottom <= docViewBottom) && (elTop >= docViewTop)) {
        inView = true;
      }
      return inView;
    },

    /**
     * Put jQuery selected elements into an array
     * @param {jQuery} $el - jQuery selection
     * @returns {Array} newArray - array of jQuery selected elements
     */
    setAsArray: function($el) {
      let newArray = [];
      $.each($el, function(i, v) {
        newArray.push($(v));
      });
      return newArray;
    },

    /**
     * Checks if the user is using IE browsers user agent string.
     */
    isIe: (function() {
      // Detects IE
      if(navigator.appName === 'Microsoft Internet Explorer' ||
        navigator.userAgent.indexOf('Trident/') > -1 ||
        /(Edge\/\d+)/.test(navigator.userAgent)) {
        return true;
      }
      return false;
    })()
  };
})(jQuery, window);
