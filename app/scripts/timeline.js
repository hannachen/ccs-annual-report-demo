'use strict';
var timeline = (function ($) {

  const $document = $(document),
        $window = $(window),
        $htmlBody = $('html, body');

  var isMobile = window.matchMedia('screen and (max-width: 48em)').matches,
      mobileOffset = 65,
      desktopOffset = 85,
      topOffset = isMobile ? mobileOffset : desktopOffset;

  var isInitialized = false;
  var mainTL, stage, renderer;
  var $tlContainer;
  var timelineWidth = 0;
  var resizeTimer;
  var self = this;
  var scaleContainer;
  var moveContainer;
  var contentHeight = 400;
  var timelinePercentage = 1;
  var timelinePercentageStart = 1;
  var timeTextMin = 1945;
  var timeTextMax = 2015;
  var timeDiff = timeTextMax - timeTextMin;
  var touchStartX = 0;
  var touchEndX = 0;
  var touchMoveDis = 0;
  var bubbleIconClickTimeout;
  var bubbleIconClickDelayFlag = false;

  var yearTimes = [
    {perc: 0, year: "1940s"},
    {perc: 0.2, year: "2000s"},
    {perc: 0.73, year: "2015"},
    {perc: 1, year: "2015"}
  ];

  var $scrubberTimeText = $('.timeline-time');
  var $tlGrabber = $('.timeline-grabber');
  var animationCheckPoints = [];
  var animationCheckDeBounceCount = 0;
  var animationCheckDeBounceCountLimit = 50;

  var viewXCenter = 0;
  var activationRegionPerc = 0.13;
  var debugFlag = false;

  var beginningAnimationFlag = false;
  var moveContainerMaxPos = 0;
  var moveContainerMinPos = 0;
  var moveContainerCurrentPos = new PIXI.Point(0,0);
  var moveContainerTarget = 0;
  var moveContainerDist = 0;
  var moveContainerPositionUpdateFlag = false;
  //meant to debounce the scrubber
  var scrubTimer;


  var staticBackgroundContainer;
  var section1Container,section2Container,section3Container,section4Container,section5Container, overpassSection4Container,section6Container;

  var loader;

  var section1_0Sprite, section1_1Sprite, section1_2Sprite, section2_1Sprite, section2_1BehindSprite, section2_2Sprite, section3_1Sprite, section3_2Sprite, section4_1Sprite, section4_2Sprite, section5_1Sprite, section5_2Sprite, overpassSection4Sprite, section6_1Sprite, section6_2Sprite, section6_2BehindSprite;

  var exploreBubbleSprite;
  var exploreBubbleContainer;
  var exploreBubbleHitarea;

  var finalBubbleSprite;
  var finalBubbleContainer;
  var finalBubbleHitarea;

  var section1TextSprite,section5TextSprite;

  var oldCoastCarContainer;
  var oldCoastCarSprite;
  var oldCoastCarTL;

  var tractorContainer;
  var tractorSprite;
  var tractorHopNestContainer;
  var tractorTL;

  var gooseContainer;
  var gooseSprite;
  var gooseHopNestContainer;
  var gooseTL;

  var geeseFlockSprite;

  var CCSLogoUpSprite1;
  var CCSLogoDownSprite2;
  var CCSLogoDownSprite3;

  var pacificTextContainer;
  var pacificTextSprite;
  var pacificMoveContainer;
  var pacificMask;

  var richmondTextContainer;
  var richmondTextSprite;
  var richmondMoveContainer;
  var richmondMask;

  var surreyMetroTextContainer;
  var surreyMetroTextSprite;
  var surreyMetroMoveContainer;
  var surreyMetroMask;
  var coastBusinessSignTL;

  var trampolineKid1Sprite;
  var trampolineKid2Sprite;
  var trampolineKid1Container;
  var trampolineKid2Container;
  var trampolineKid1TL;
  var trampolineKid2TL;

  var wifiWave1Sprite,wifiWave2Sprite,wifiWave3Sprite,wifiWave4Sprite,wifiWave5Sprite,wifiWave6Sprite;
  var wifiWave1Container,wifiWave2Container,wifiWave3Container,wifiWave4Container,wifiWave5Container,wifiWave6Container;
  var wifiWave1TL,wifiWave2TL,wifiWave3TL,wifiWave4TL,wifiWave5TL,wifiWave6TL;

  var garySprite;
  var garyTL;

  var redCarSprite, redCar2Sprite;
  var redCarContainer, redCar2Container;
  var redCarTL, redCar2TL;

  var skytrain1Sprite,skytrain2Sprite;
  var skytrain1Container, skytrain2Container;
  var skytrain1TL, skytrain2TL;

  var securityTruckSprite;
  var securityTruckContainer;
  var securityTruckTL;

  var blueCarSprite, blueCar2Sprite, blueCar3Sprite;
  var blueCarContainer, blueCar2Container, blueCar3Container;
  var blueCarTL, blueCar2TL, blueCar3TL;


  var graduateStudent1Sprite,graduateStudent2Sprite,graduateStudent3Sprite;
  var graduateStudent1HatSprite;
  var graduateStudentsContainer;

  var centerHeight = contentHeight*0.5;
  var bubbleTimelineArray;
  var autoPlayScrollTL;

  var autoPlayTimeoutInterval;
  var autoPlayingFlag = false;

  var debugViewContainer;
  var debugleftBorder, debugrightBorder, debugcenterBorder;
  var debugActivationRegion;
  var debugTargetXDisplay;
  /**
   * Initialize the timeline.
   */
  function init() {
    // console.log('timeline initialized');

    $tlContainer = $('.timeline-container');
    mainTL = new TimelineMax({paused:true});
    stage = new PIXI.Container();
    renderer = PIXI.autoDetectRenderer($tlContainer.width(),$tlContainer.height(),{backgroundColor: 0xb1d45c, resolution: window.devicePixelRatio, autoResize: true});
    renderer.plugins.interaction.autoPreventDefault = false;
    var bF = 'images/timeline-assets/';
    loader = PIXI.loader;
    loader.add('section1_0',bF+'section-1-0@2x.png');
    loader.add('section1_1',bF+'section-1-1@2x.png');
    loader.add('section1_2',bF+'section-1-2@2x.png');
    loader.add('section2_1',bF+'section-2-1@2x.png');
    loader.add('section2_1_Behind',bF+'section-2-1-behind@2x.png');
    loader.add('section2_2',bF+'section-2-2@2x.png');
    loader.add('section3_1',bF+'section-3-1@2x.png');
    loader.add('section3_2',bF+'section-3-2@2x.png');
    loader.add('section4_1',bF+'section-4-1@2x.png');
    loader.add('section4_2',bF+'section-4-2@2x.png');
    loader.add('overpass_section_4',bF+'overpass@2x.png');
    loader.add('section5_1',bF+'section-5-1@2x.png');
    loader.add('section5_2',bF+'section-5-2@2x.png');
    loader.add('section6_1',bF+'section-6-1@2x.png');
    loader.add('section6_2',bF+'section-6-2@2x.png');
    loader.add('section6_2_Behind',bF+'section-6-2-behind@2x.png');

    loader.add('exploreBubble',bF+'text/explore-bubble@2x.png');
    loader.add('finalBubble',bF+'text/final-bubble@2x.png');
    loader.add('section1Text',bF+'text/section1-text@2x.png');
    loader.add('section5Text',bF+'text/section5-text@2x.png');
    loader.add('bubbleIcon',bF+'text/bubble_icon@2x.png');
    loader.add('bubbleText1',bF+'text/bubbleText_1@2x.png');
    loader.add('bubbleText2',bF+'text/bubbleText_2@2x.png');
    loader.add('bubbleText3',bF+'text/bubbleText_3@2x.png');
    loader.add('bubbleText4',bF+'text/bubbleText_4@2x.png');
    loader.add('bubbleText5',bF+'text/bubbleText_5@2x.png');
    loader.add('bubbleText6',bF+'text/bubbleText_6@2x.png');
    loader.add('bubbleText7',bF+'text/bubbleText_7@2x.png');
    loader.add('bubbleText8',bF+'text/bubbleText_8@2x.png');
    loader.add('bubbleText9',bF+'text/bubbleText_9@2x.png');
    loader.add('bubbleText10',bF+'text/bubbleText_10@2x.png');
    loader.add('bubbleText11',bF+'text/bubbleText_11@2x.png');
    loader.add('bubbleText12',bF+'text/bubbleText_12@2x.png');
    loader.add('bubbleText13',bF+'text/bubbleText_13@2x.png');
    loader.add('bubbleText14',bF+'text/bubbleText_14@2x.png');

    loader.add('oldCoastCar',bF+'old_ccs_car@2x.png');
    loader.add('tractor',bF+'tractor@2x.png');
    loader.add('goose',bF+'single_goose@2x.png');
    loader.add('geeseFlock',bF+'geese_flock@2x.png');
    loader.add('CCSLogoUp',bF+'coast_sign_up@2x.png');
    loader.add('CCSLogoDown',bF+'coast_sign_down@2x.png');
    loader.add('pacificText',bF+'pacific_coast_savings_text@2x.png');
    loader.add('richmondText',bF+'richmond_savings_text@2x.png');
    loader.add('surreyMetroText',bF+'surrey_metro_savings_text@2x.png');
    loader.add('trampolineKid1',bF+'trampoline_kid_1@2x.png');
    loader.add('trampolineKid2',bF+'trampoline_kid_2@2x.png');
    loader.add('wifiWave',bF+'wifi_signal@2x.png');
    loader.add('gary',bF+'gary_guy@2x.png');
    loader.add('redCar',bF+'red_car@2x.png');
    loader.add('skytrain',bF+'skytrain@2x.png');
    loader.add('securityTruck',bF+'security_truck@2x.png');
    loader.add('blueCar',bF+'blue_car@2x.png');
    loader.add('graduateStudent1',bF+'graduate_student1@2x.png');
    loader.add('graduateStudent1Hat',bF+'graduate1_hat@2x.png');
    loader.add('graduateStudent2',bF+'graduate_student2@2x.png');
    loader.add('graduateStudent3',bF+'graduate_student3@2x.png');

    loader.once('complete', onAssetsLoaded);
    loader.load();

  }

  function onAssetsLoaded(loader, resources) {
    scaleContainer = new PIXI.Container();

    staticBackgroundContainer = new PIXI.Container();

    section1Container = new PIXI.Container();
    section2Container = new PIXI.Container();
    section3Container = new PIXI.Container();
    section4Container = new PIXI.Container();
    overpassSection4Container = new PIXI.Container();
    section5Container = new PIXI.Container();
    section6Container = new PIXI.Container();
    staticBackgroundContainer.addChild(section6Container);
    staticBackgroundContainer.addChild(section5Container);
    staticBackgroundContainer.addChild(section4Container);
    staticBackgroundContainer.addChild(section3Container);
    staticBackgroundContainer.addChild(section2Container);
    staticBackgroundContainer.addChild(section1Container);

    var beginningRunnoff = new PIXI.Graphics();
    beginningRunnoff.beginFill(0xd9e8ab);
    beginningRunnoff.drawRect(-500,0,500,450);
    beginningRunnoff.endFill();
    beginningRunnoff.position = new PIXI.Point(-585,0);

    section1Container.position = new PIXI.Point(0,0);
    section1_0Sprite = new PIXI.Sprite(resources.section1_0.texture);
    section1_0Sprite.position = new PIXI.Point(-585,0);
    section1_1Sprite = new PIXI.Sprite(resources.section1_1.texture);
    section1_2Sprite = new PIXI.Sprite(resources.section1_2.texture);
    section1_2Sprite.position = new PIXI.Point(1292,0);
    section1Container.addChild(beginningRunnoff);
    section1Container.addChild(section1_0Sprite);
    section1Container.addChild(section1_1Sprite);
    section1Container.addChild(section1_2Sprite);
    section1TextSprite = new PIXI.Sprite(resources.section1Text.texture);
    section1TextSprite.position = new PIXI.Point(250,100);
    section1TextSprite.alpha = 0;
    section1Container.addChild(section1TextSprite);
    var section1TextTL = new TimelineMax({paused:true});
    section1TextTL.to(section1TextSprite,1,{alpha:1,delay:1});

    createAnimationCheckPoint(section1TextTL, 382, 300, 2548, 0x3F4462, false);

    blueCar2Sprite = new PIXI.Sprite(resources.blueCar.texture);
    blueCar2Container = new PIXI.Container();
    blueCar2Container.position = new PIXI.Point(1125,400);
    blueCar2Container.addChild(blueCar2Sprite);

    blueCar2TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    blueCar2TL.to(blueCar2Sprite,6,{x:-1040,y:-600,delay:3,ease:Linear.easeNone});

    section2Container.position = new PIXI.Point(1850,0);
    section2_1Sprite = new PIXI.Sprite(resources.section2_1.texture);
    section2_1BehindSprite = new PIXI.Sprite(resources.section2_1_Behind.texture);
    section2_2Sprite = new PIXI.Sprite(resources.section2_2.texture);
    section2_1BehindSprite.position = new PIXI.Point(343,0);
    section2_2Sprite.position = new PIXI.Point(1382,0);
    section2Container.addChild(section2_1BehindSprite);
    section2Container.addChild(blueCar2Container);
    section2Container.addChild(section2_1Sprite);
    section2Container.addChild(section2_2Sprite);

    redCar2Sprite = new PIXI.Sprite(resources.redCar.texture);
    redCar2Container = new PIXI.Container();
    redCar2Container.position = new PIXI.Point(-40,-100);
    redCar2Container.addChild(redCar2Sprite);

    redCar2TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    redCar2TL.to(redCar2Sprite,7,{x:1050,y:600, ease:Linear.easeNone,delay:2});

    section3Container.position = new PIXI.Point(3720,0);
    section3_1Sprite = new PIXI.Sprite(resources.section3_1.texture);
    section3_2Sprite = new PIXI.Sprite(resources.section3_2.texture);
    section3_2Sprite.position = new PIXI.Point(1058,0);
    section3Container.addChild(section3_1Sprite);
    section3Container.addChild(section3_2Sprite);
    section3Container.addChild(redCar2Container);

    section4Container.position = new PIXI.Point(5050,0);
    section4_1Sprite = new PIXI.Sprite(resources.section4_1.texture);
    section4_2Sprite = new PIXI.Sprite(resources.section4_2.texture);
    section4_2Sprite.position = new PIXI.Point(1229,0);
    section4Container.addChild(section4_1Sprite);
    section4Container.addChild(section4_2Sprite);

    overpassSection4Container.position = new PIXI.Point(5888,0);
    overpassSection4Sprite = new PIXI.Sprite(resources.overpass_section_4.texture);
    overpassSection4Container.addChild(overpassSection4Sprite);

    section5Container.position = new PIXI.Point(6960,0);
    section5_1Sprite = new PIXI.Sprite(resources.section5_1.texture);
    section5_2Sprite = new PIXI.Sprite(resources.section5_2.texture);
    section5_2Sprite.position = new PIXI.Point(1350,0);
    section5Container.addChild(section5_1Sprite);
    section5Container.addChild(section5_2Sprite);

    section5TextSprite = new PIXI.Sprite(resources.section5Text.texture);
    section5TextSprite.position = new PIXI.Point(150,130);
    section5TextSprite.alpha = 0;
    section5Container.addChild(section5TextSprite);
    var section5TextTL = new TimelineMax({paused:true});
    section5TextTL.to(section5TextSprite,1,{alpha:1});

    createAnimationCheckPoint(section5TextTL, 7226, 500, 2496, 0x3F4462, false);

    blueCar3Sprite = new PIXI.Sprite(resources.blueCar.texture);
    blueCar3Container = new PIXI.Container();
    blueCar3Container.position = new PIXI.Point(2480,400);
    blueCar3Container.addChild(blueCar3Sprite);

    blueCar3TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    blueCar3TL.to(blueCar3Sprite,6,{x:-1040,y:-600,delay:2,ease:Linear.easeNone});

    section6Container.position = new PIXI.Point(9020,0);
    section6_1Sprite = new PIXI.Sprite(resources.section6_1.texture);
    section6_2Sprite = new PIXI.Sprite(resources.section6_2.texture);
    section6_2BehindSprite = new PIXI.Sprite(resources.section6_2_Behind.texture);
    section6_2Sprite.position = new PIXI.Point(1162,0);
    section6_2BehindSprite.position = new PIXI.Point(1680,0);
    section6Container.addChild(section6_2BehindSprite);
    section6Container.addChild(blueCar3Container);
    section6Container.addChild(section6_1Sprite);
    section6Container.addChild(section6_2Sprite);


    exploreBubbleSprite = new PIXI.Sprite(resources.exploreBubble.texture);
    exploreBubbleContainer = new PIXI.Container();
    exploreBubbleContainer.position = new PIXI.Point(1365,100);
    exploreBubbleContainer.scale.set(1);
    exploreBubbleContainer.addChild(exploreBubbleSprite);
    exploreBubbleHitarea = new PIXI.Graphics();
    exploreBubbleHitarea.beginFill(0x000000,0);
    exploreBubbleHitarea.drawRect(0,0,230,42);
    exploreBubbleHitarea.endFill();
    exploreBubbleHitarea.position = new PIXI.Point(38,113);
    exploreBubbleHitarea.interactive = true;
    exploreBubbleHitarea.buttonMode = true;
    exploreBubbleHitarea.mousedown = function(e) {
      gotoBeginning(false);
      clearTimeout(autoPlayTimeoutInterval);
    }
    exploreBubbleHitarea.touchstart = function(e) {
      gotoBeginning(false);
      clearTimeout(autoPlayTimeoutInterval);
    }
    exploreBubbleHitarea.click = function() {
      $document.trigger('click.explore');
    }
    exploreBubbleContainer.addChild(exploreBubbleHitarea);
    section6Container.addChild(exploreBubbleContainer);



    finalBubbleSprite = new PIXI.Sprite(resources.finalBubble.texture);
    finalBubbleContainer = new PIXI.Container();
    finalBubbleContainer.position = new PIXI.Point(1395,90);
    finalBubbleContainer.scale.set(1);
    finalBubbleContainer.addChild(finalBubbleSprite);
    finalBubbleHitarea = new PIXI.Graphics();
    finalBubbleHitarea.beginFill(0x000000,0);
    finalBubbleHitarea.drawRect(0,0,45,45);
    finalBubbleHitarea.endFill();
    finalBubbleHitarea.position = new PIXI.Point(100,95);
    finalBubbleHitarea.interactive = true;
    finalBubbleHitarea.buttonMode = true;
    finalBubbleHitarea.mousedown = function(e) {
      scrollDownToNextSection();
    }
    finalBubbleHitarea.touchstart = function(e) {
      scrollDownToNextSection();
    }
    finalBubbleContainer.visible = false;
    finalBubbleContainer.addChild(finalBubbleHitarea);
    section6Container.addChild(finalBubbleContainer);

    //the old car
    oldCoastCarSprite = new PIXI.Sprite(resources.oldCoastCar.texture);
    oldCoastCarContainer = new PIXI.Container();
    oldCoastCarContainer.addChild(oldCoastCarSprite);
    oldCoastCarContainer.position = new PIXI.Point(120,250);
    oldCoastCarTL = new TimelineMax({paused:true});
    oldCoastCarTL.to(oldCoastCarSprite,10,{x:160,y:100});

    createAnimationCheckPoint(oldCoastCarTL, 382, 500, 2548, 0x4275A3, false);

    gooseSprite = new PIXI.Sprite(resources.goose.texture);
    gooseHopNestContainer = new PIXI.Container();
    gooseContainer = new PIXI.Container();

    gooseHopNestContainer.addChild(gooseSprite);
    gooseContainer.addChild(gooseHopNestContainer);

    gooseContainer.position = new PIXI.Point(1430,140);
    gooseTL = new TimelineMax({paused:true});
    gooseTL.to(gooseHopNestContainer,11,{x:280,y:170});

    var gooseTLHop = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    gooseTLHop.to(gooseSprite,0.2,{y:-5,ease:Sine.easeOut});
    gooseTLHop.to(gooseSprite,0.2,{y:0,ease:Sine.easeIn});
    gooseTLHop.progress(Math.random());

    createAnimationCheckPoint(gooseTL, 1651, 500, 2548, 0xCFC085, false);

    //the tractor
    tractorSprite = new PIXI.Sprite(resources.tractor.texture);
    tractorContainer = new PIXI.Container();
    tractorHopNestContainer = new PIXI.Container();

    tractorHopNestContainer.addChild(tractorSprite)
    tractorContainer.addChild(tractorHopNestContainer);
    tractorContainer.position = new PIXI.Point(1470,145);
    tractorTL = new TimelineMax({paused:true});
    tractorTL.to(tractorHopNestContainer,10,{x:290,y:170});

    var tractorTLHop = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    tractorTLHop.to(tractorSprite,0.3,{y:-2,ease:Sine.easeOut});
    tractorTLHop.to(tractorSprite,0.3,{y:0,ease:Sine.easeIn});
    tractorTLHop.progress(Math.random());

    createAnimationCheckPoint(tractorTL, 1651, 500, 2548, 0xCFC085, false);

    geeseFlockSprite = new PIXI.Sprite(resources.geeseFlock.texture);
    geeseFlockSprite.position = new PIXI.Point(1800,120);

    //all the signs
    CCSLogoUpSprite1 = new PIXI.Sprite(resources.CCSLogoUp.texture);
    CCSLogoDownSprite2 = new PIXI.Sprite(resources.CCSLogoDown.texture);
    CCSLogoDownSprite3 = new PIXI.Sprite(resources.CCSLogoDown.texture);

    pacificTextSprite = new PIXI.Sprite(resources.pacificText.texture);
    richmondTextSprite = new PIXI.Sprite(resources.richmondText.texture);
    surreyMetroTextSprite = new PIXI.Sprite(resources.surreyMetroText.texture);

    pacificTextSprite.position = new PIXI.Point(16,-15);
    CCSLogoUpSprite1.position = new PIXI.Point(-55,52);
    pacificTextContainer = new PIXI.Container();
    pacificMoveContainer = new PIXI.Container();
    pacificMoveContainer.addChild(pacificTextSprite);
    pacificMoveContainer.addChild(CCSLogoUpSprite1);
    pacificTextContainer.addChild(pacificMoveContainer);
    pacificTextContainer.position = new PIXI.Point(2513,205);
    pacificMask = new PIXI.Graphics();
    pacificMask.isMask = true;
    pacificMask.beginFill(0x000000,0.5);
    pacificMask.drawRect(0,-35,98,110);
    pacificMask.endFill();
    pacificTextContainer.addChild(pacificMask);
    pacificTextContainer.mask = pacificMask;

    richmondTextSprite.position = new PIXI.Point(18,21);
    CCSLogoDownSprite2.position = new PIXI.Point(-56,-15);
    richmondTextContainer = new PIXI.Container();
    richmondMoveContainer = new PIXI.Container();
    richmondMoveContainer.addChild(richmondTextSprite);
    richmondMoveContainer.addChild(CCSLogoDownSprite2);
    richmondTextContainer.addChild(richmondMoveContainer);
    richmondTextContainer.position = new PIXI.Point(2620,60);
    richmondMask = new PIXI.Graphics();
    richmondMask.isMask = true;
    richmondMask.beginFill(0x000000,0.5);
    richmondMask.drawRect(0,-3,95,110);
    richmondMask.endFill();
    richmondTextContainer.addChild(richmondMask);
    richmondTextContainer.mask = richmondMask;

    surreyMetroTextSprite.position = new PIXI.Point(12,17);
    CCSLogoDownSprite3.position = new PIXI.Point(-56,-15);
    surreyMetroTextContainer = new PIXI.Container();
    surreyMetroMoveContainer = new PIXI.Container();
    surreyMetroMoveContainer.addChild(surreyMetroTextSprite);
    surreyMetroMoveContainer.addChild(CCSLogoDownSprite3);
    surreyMetroTextContainer.addChild(surreyMetroMoveContainer);
    surreyMetroTextContainer.position = new PIXI.Point(2900,205);
    surreyMetroMask = new PIXI.Graphics();
    surreyMetroMask.isMask = true;
    surreyMetroMask.beginFill(0x000000,0.5);
    surreyMetroMask.drawRect(0,-3,95,110);
    surreyMetroMask.endFill();
    surreyMetroTextContainer.addChild(surreyMetroMask);
    surreyMetroTextContainer.mask = surreyMetroMask;

    coastBusinessSignTL = new TimelineMax({paused:true, delay:1});
    coastBusinessSignTL.add([
      TweenMax.to(pacificMoveContainer,2.5,{x:90,y:-50}),
      TweenMax.to(richmondMoveContainer,2.5,{x:90,y:50,delay:1}),
      TweenMax.to(surreyMetroMoveContainer,2.5,{x:90,y:50,delay:2})
    ]);

    createAnimationCheckPoint(coastBusinessSignTL, 2742, 500, 2128, 0x426398, false);

    trampolineKid1Sprite = new PIXI.Sprite(resources.trampolineKid1.texture);
    trampolineKid2Sprite = new PIXI.Sprite(resources.trampolineKid2.texture);
    trampolineKid1Container = new PIXI.Container();
    trampolineKid1Container.addChild(trampolineKid1Sprite);
    trampolineKid2Container = new PIXI.Container();
    trampolineKid2Container.addChild(trampolineKid2Sprite);

    trampolineKid1Container.position = new PIXI.Point(3320,240);
    trampolineKid2Container.position = new PIXI.Point(3340,240);

    trampolineKid1TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    trampolineKid1TL.to(trampolineKid1Sprite,0.5,{y:-20,ease:Power2.easeOut});
    trampolineKid1TL.to(trampolineKid1Sprite,0.5,{y:0,ease:Power2.easeIn});

    trampolineKid2TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    trampolineKid2TL.to(trampolineKid2Sprite,0.6,{y:-23,ease:Power2.easeOut});
    trampolineKid2TL.to(trampolineKid2Sprite,0.6,{y:0,ease:Power2.easeIn});


    wifiWave1Sprite = new PIXI.Sprite(resources.wifiWave.texture);
    wifiWave1Sprite.anchor = new PIXI.Point(1,1);
    wifiWave1Container = new PIXI.Container();
    wifiWave1Container.addChild(wifiWave1Sprite);
    wifiWave1Container.scale.set(0);
    wifiWave1Container.alpha = 0;
    wifiWave1Container.position = new PIXI.Point(4455,123);
    wifiWave1TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});

    wifiWave1TL.add([
      TweenMax.to(wifiWave1Container,1,{alpha:1}),
      TweenMax.to(wifiWave1Container,1,{alpha:0,delay:0.5}),
      TweenMax.to(wifiWave1Container.scale,1,{x:1.5,y:1.5})
    ]);

    wifiWave2Sprite = new PIXI.Sprite(resources.wifiWave.texture);
    wifiWave2Sprite.anchor = new PIXI.Point(1,1);
    wifiWave2Container = new PIXI.Container();
    wifiWave2Container.addChild(wifiWave2Sprite);
    wifiWave2Container.scale.set(-1,1);
    wifiWave2Container.alpha = 0;
    //7925
    //192
    wifiWave2Container.position = new PIXI.Point(8117,37);
    wifiWave2TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});

    wifiWave2TL.add([
      TweenMax.to(wifiWave2Container,1,{alpha:1}),
      TweenMax.to(wifiWave2Container,1,{alpha:0,delay:0.5}),
      TweenMax.to(wifiWave2Container.scale,1,{x:-1.5,y:1.5})
    ]);
    wifiWave2TL.progress(Math.random());

    wifiWave3Sprite = new PIXI.Sprite(resources.wifiWave.texture);
    wifiWave3Sprite.anchor = new PIXI.Point(1,1);
    wifiWave3Container = new PIXI.Container();
    wifiWave3Container.addChild(wifiWave3Sprite);
    wifiWave3Container.scale.set(1,1);
    wifiWave3Container.alpha = 0;
    wifiWave3Container.position = new PIXI.Point(8150,58);
    wifiWave3TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});

    wifiWave3TL.add([
      TweenMax.to(wifiWave3Container,1,{alpha:1}),
      TweenMax.to(wifiWave3Container,1,{alpha:0,delay:0.5}),
      TweenMax.to(wifiWave3Container.scale,1,{x:1.5,y:1.5})
    ]);
    wifiWave3TL.progress(Math.random());

    wifiWave4Sprite = new PIXI.Sprite(resources.wifiWave.texture);
    wifiWave4Sprite.anchor = new PIXI.Point(1,1);
    wifiWave4Container = new PIXI.Container();
    wifiWave4Container.addChild(wifiWave4Sprite);
    wifiWave4Container.scale.set(1,1);
    wifiWave4Container.alpha = 0;
    wifiWave4Container.position = new PIXI.Point(8180,40);
    wifiWave4TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});

    wifiWave4TL.add([
      TweenMax.to(wifiWave4Container,1,{alpha:1}),
      TweenMax.to(wifiWave4Container,1,{alpha:0,delay:0.5}),
      TweenMax.to(wifiWave4Container.scale,1,{x:1.5,y:1.5})
    ]);
    wifiWave4TL.progress(Math.random());

    wifiWave5Sprite = new PIXI.Sprite(resources.wifiWave.texture);
    wifiWave5Sprite.anchor = new PIXI.Point(1,1);
    wifiWave5Container = new PIXI.Container();
    wifiWave5Container.addChild(wifiWave5Sprite);
    wifiWave5Container.scale.set(1,1);
    wifiWave5Container.alpha = 0;
    wifiWave5Container.position = new PIXI.Point(8225,35);
    wifiWave5TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});

    wifiWave5TL.add([
      TweenMax.to(wifiWave5Container,1,{alpha:1}),
      TweenMax.to(wifiWave5Container,1,{alpha:0,delay:0.5}),
      TweenMax.to(wifiWave5Container.scale,1,{x:1.5,y:1.5})
    ]);
    wifiWave5TL.progress(Math.random());

    wifiWave6Sprite = new PIXI.Sprite(resources.wifiWave.texture);
    wifiWave6Sprite.anchor = new PIXI.Point(1,1);
    wifiWave6Container = new PIXI.Container();
    wifiWave6Container.addChild(wifiWave6Sprite);
    wifiWave6Container.scale.set(-1,1);
    wifiWave6Container.alpha = 0;
    wifiWave6Container.position = new PIXI.Point(8187,83);
    wifiWave6TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});

    wifiWave6TL.add([
      TweenMax.to(wifiWave6Container,1,{alpha:1}),
      TweenMax.to(wifiWave6Container,1,{alpha:0,delay:0.5}),
      TweenMax.to(wifiWave6Container.scale,1,{x:-1.5,y:1.5})
    ]);
    wifiWave6TL.progress(Math.random());

    redCarSprite = new PIXI.Sprite(resources.redCar.texture);
    redCarContainer = new PIXI.Container();
    redCarContainer.position = new PIXI.Point(5260,-100);
    redCarContainer.addChild(redCarSprite);

    redCarTL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    redCarTL.to(redCarSprite,7,{x:1050,y:600, ease:Linear.easeNone,delay:2});



    garySprite = new PIXI.Sprite(resources.gary.texture);
    garySprite.anchor = new PIXI.Point(0.5,0);
    garySprite.position = new PIXI.Point(5953,46);
    garySprite.rotation = 0.1;

    garyTL = new TimelineMax({onComplete: function() {
      this.restart();
    }});
    garyTL.to(garySprite,1,{rotation:-0.1, ease:Power2.easeInOut});
    garyTL.to(garySprite,1,{rotation:0.1, ease:Power2.easeInOut});


    securityTruckSprite = new PIXI.Sprite(resources.securityTruck.texture);
    securityTruckContainer = new PIXI.Container();
    securityTruckContainer.position = new PIXI.Point(6650,140);
    securityTruckContainer.addChild(securityTruckSprite);

    securityTruckTL = new TimelineMax({paused:true});
    securityTruckTL.to(securityTruckSprite,3,{x:200,y:100,ease:Power1.easeIn,delay:0.5});
    securityTruckTL.to(securityTruckSprite,2.5,{x:550,y:300,ease:Linear.easeNone});

    createAnimationCheckPoint(securityTruckTL, 6711, 500, 2548, 0xE0DDD8, false);


    blueCarSprite = new PIXI.Sprite(resources.blueCar.texture);
    blueCarContainer = new PIXI.Container();
    blueCarContainer.position = new PIXI.Point(7310,470);
    blueCarContainer.addChild(blueCarSprite);

    blueCarTL = new TimelineMax({onComplete:function(){
      this.restart();
    }});
    blueCarTL.progress(0.2);
    blueCarTL.to(blueCarSprite,6,{x:-1040,y:-600,delay:3,ease:Linear.easeNone});


    skytrain1Sprite = new PIXI.Sprite(resources.skytrain.texture);
    skytrain1Sprite.scale.set(-1,1);
    skytrain1Sprite.anchor = new PIXI.Point(1,1);
    skytrain1Container = new PIXI.Container();
    skytrain1Container.position = new PIXI.Point(6650,0)
    skytrain1Container.addChild(skytrain1Sprite);

    skytrain1TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    skytrain1TL.to(skytrain1Sprite,7,{x:-1560,y:900,delay:3.5, ease:Linear.easeNone});


    skytrain2Sprite = new PIXI.Sprite(resources.skytrain.texture);
    skytrain2Sprite.anchor = new PIXI.Point(1,1);
    skytrain2Container = new PIXI.Container();
    skytrain2Container.position = new PIXI.Point(7346,0);
    skytrain2Container.addChild(skytrain2Sprite);

    skytrain2TL = new TimelineMax({onComplete:function() {
      this.restart();
    }});
    skytrain2TL.progress(0.5);
    skytrain2TL.to(skytrain2Sprite,7,{x:1550,y:900,delay:3, ease:Linear.easeNone});

    graduateStudent1Sprite = new PIXI.Sprite(resources.graduateStudent1.texture);
    graduateStudent1Sprite.anchor = new PIXI.Point(1,0.5);

    graduateStudent1HatSprite = new PIXI.Sprite(resources.graduateStudent1Hat.texture);
    graduateStudent1HatSprite.anchor = new PIXI.Point(0.5,0.5);
    graduateStudent1HatSprite.position = new PIXI.Point(-18, -18);
    graduateStudent2Sprite = new PIXI.Sprite(resources.graduateStudent2.texture);
    graduateStudent2Sprite.anchor = new PIXI.Point(1,0.5);
    graduateStudent2Sprite.position = new PIXI.Point(25, 0);
    graduateStudent3Sprite = new PIXI.Sprite(resources.graduateStudent3.texture);
    graduateStudent3Sprite.anchor = new PIXI.Point(1,0.5);
    graduateStudent3Sprite.position = new PIXI.Point(50, 0);

    var gradHatTL = new TimelineMax({onComplete:function(){
      this.restart();
    }, onUpdate: function() {
      graduateStudent1HatSprite.rotation -= 0.1;
    }});
    gradHatTL.to(graduateStudent1HatSprite,0.5,{y:-40, ease:Sine.easeOut});
    gradHatTL.to(graduateStudent1HatSprite,0.5,{y:-18, ease:Sine.easeIn});

    var gradStudent1TL = new TimelineMax({onComplete:function(){
      this.restart();
    }});
    gradStudent1TL.to(graduateStudent1Sprite,0.2,{y:-5, ease:Sine.easeOut});
    gradStudent1TL.to(graduateStudent1Sprite,0.2,{y:0, ease:Sine.easeIn});
    gradStudent1TL.progress(Math.random());

    var gradStudent2TL = new TimelineMax({onComplete:function(){
      this.restart();
    }});
    gradStudent2TL.to(graduateStudent2Sprite,0.3,{y:-5, ease:Sine.easeOut});
    gradStudent2TL.to(graduateStudent2Sprite,0.3,{y:0, ease:Sine.easeIn});
    gradStudent2TL.progress(Math.random());

    var gradStudent3TL = new TimelineMax({onComplete:function(){
      this.restart();
    }});
    gradStudent3TL.to(graduateStudent3Sprite,0.25,{y:-5, ease:Sine.easeOut});
    gradStudent3TL.to(graduateStudent3Sprite,0.25,{y:0, ease:Sine.easeIn});
    gradStudent3TL.progress(Math.random());

    graduateStudentsContainer = new PIXI.Container();
    graduateStudentsContainer.position = new PIXI.Point(10280,330);


    graduateStudentsContainer.addChild(graduateStudent1Sprite);
    graduateStudentsContainer.addChild(graduateStudent1HatSprite);
    graduateStudentsContainer.addChild(graduateStudent2Sprite);
    graduateStudentsContainer.addChild(graduateStudent3Sprite);


    //adding everything to the move container
    moveContainer = new PIXI.Container();
    moveContainer.addChild(staticBackgroundContainer);
    moveContainer.addChild(oldCoastCarContainer);
    moveContainer.addChild(geeseFlockSprite);
    moveContainer.addChild(gooseContainer);
    moveContainer.addChild(tractorContainer);
    moveContainer.addChild(pacificTextContainer);
    moveContainer.addChild(richmondTextContainer);
    moveContainer.addChild(surreyMetroTextContainer);
    moveContainer.addChild(trampolineKid1Container);
    moveContainer.addChild(trampolineKid2Container);
    moveContainer.addChild(wifiWave1Container);
    moveContainer.addChild(redCarContainer);
    moveContainer.addChild(garySprite);
    moveContainer.addChild(blueCarContainer);
    moveContainer.addChild(securityTruckContainer);
    moveContainer.addChild(overpassSection4Container);
    moveContainer.addChild(skytrain1Container);
    moveContainer.addChild(skytrain2Container);
    moveContainer.addChild(wifiWave2Container);
    moveContainer.addChild(wifiWave3Container);
    moveContainer.addChild(wifiWave4Container);
    moveContainer.addChild(wifiWave5Container);
    moveContainer.addChild(wifiWave6Container);
    moveContainer.addChild(graduateStudentsContainer);

    var bbl1TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText1.texture,780,190, moveContainer,-92.25, -76);
    var bbl2TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText2.texture,1196,120, moveContainer,-92.25, -58.25, 90);
    var bbl3TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText3.texture,2020,210, moveContainer,-92.25, -66.75);
    var bbl4TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText4.texture,2780,270, moveContainer,-98.25, -93.25);
    var bbl5TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText5.texture,3860,200, moveContainer,-92.25, -68.25);
    var bbl6TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText6.texture,4500,210, moveContainer,-92.25, -90.25);
    var bbl7TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText7.texture,5925,127, moveContainer,-92.25, -83.75);
    var bbl8TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText8.texture,8015,190, moveContainer,-90, -75.25);
    var bbl9TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText9.texture,8250,80, moveContainer,-97, -75.25, 110, -70);
    var bbl10TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText10.texture,8580,220, moveContainer, -102.25, -127.75);
    var bbl11TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText11.texture,9000,235, moveContainer, -94.75, -78.25);
    var bbl12TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText12.texture,9208,200, moveContainer, -92.25, -116.5);
    var bbl13TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText13.texture,9775,145, moveContainer, -135.25, -44.75);
    var bbl14TL = createPopupBubble(resources.bubbleIcon.texture, resources.bubbleText14.texture,10255,250, moveContainer, -80, -68);

    scaleContainer.addChild(moveContainer);
    scaleContainer.scale.set($tlContainer.height()/contentHeight);

    calculateMoveContainerMaxMinPos();
    setMoveContainerStartPos();

    stage.addChild(scaleContainer);

    updateViewXCenter();
    if(debugFlag)
      addDebugView();

    $tlContainer.append(renderer.view);

    /**
    **
    **
    ** adding in the scrubber logic
    **
    **
    **/
    $(window).on('mouseup touchend', function(){

      $('.timeline-grabber').off('mousemove touchmove');
      $('.timeline-grabber').parents().off('mousemove touchmove');
      $('.timeline-container').off('touchmove');
    });

    $('.timeline-container').on('touchstart', function(e){
      touchStartX = e.originalEvent.touches[0].clientX;
      timelinePercentageStart = timelinePercentage;
      $(this).on('touchmove', function(e) {

        moveTime(e,true);

      });
    });

    $('.timeline-grabber').on('mousedown touchstart', function() {
      if(beginningAnimationFlag)
        return;
      timelineWidth = $('.scrubber-box').width();
      autoPlayingFlag = false;

      $(this).addClass('tl-grabber-draggable').parents().on('mousemove touchmove',function(e){
        e.preventDefault();

        // clearTimeout(scrubTimer);
        // scrubTimer = setTimeout(function() {
        //
        // }, 100);

        moveTime(e,false);

    }).on('mouseup touchend', function(e) {
        $('.tl-grabber-draggable').removeClass('tl-grabber-draggable');
      });
    });
    draw();

    autoPlayTimeoutInterval = setTimeout(function() {
      gotoBeginning(true);
    },30000);

    $(window).on('resize', function(e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
        renderer.resize($tlContainer.width(),$tlContainer.height());
        scaleContainer.scale.set($tlContainer.height()/contentHeight);

        if(beginningAnimationFlag)
          return;
        calculateMoveContainerMaxMinPos();
        if(autoPlayScrollTL != null) {
          autoPlayScrollTL.kill();
          beginningAnimationFlag = false;
          autoPlayingFlag = false;
        }

        // repositions the scrubber

        updateTimeGrabberPosition(true)
        updateScrollTarget();

        updateViewXCenter();

        if(debugFlag) {
          updateDebug();
          updateDebugTargetPos();
        }

     }, 100);
    });

    TweenMax.to('#timeline',1,{opacity:1,delay:1});
  }

  function moveTime(e,canvasTouchFlag) {
    if(beginningAnimationFlag)
      return;
    var draggerCenter = $('.tl-grabber-draggable').outerWidth()/2;

    var edgeOffset = 30;

    var pageX = 0;
    var leftOffset = 0;

    if(e.originalEvent.touches){
      pageX = e.originalEvent.touches[0].pageX;
      if(canvasTouchFlag) {
        touchEndX = e.originalEvent.touches[0].clientX;
        touchMoveDis = (touchStartX - touchEndX);
      }
    } else {
      pageX = e.pageX;
    }

    leftOffset = pageX - draggerCenter - edgeOffset;

    if(leftOffset <= 0) {
      leftOffset = 0;
    } else if (leftOffset+(draggerCenter*2) >= timelineWidth) {
        leftOffset = timelineWidth-(draggerCenter*2);
    }

    if(canvasTouchFlag) {
      // console.log(touchMoveDis);
      var newPerc = timelinePercentageStart + ((-touchMoveDis * moveContainer.scale.x))/moveContainerMaxPos;
      if(newPerc >= 1) {
        newPerc = 1;
      } else if(newPerc <= 0) {
        newPerc = 0;
      }
      timelinePercentage = newPerc;
    } else {
      timelinePercentage = leftOffset/(timelineWidth-(draggerCenter*2));
    }

    updateScrubberTimeText();
    updateScrollTarget();
    updateTimeGrabberPosition(false);
    $('.tl-grabber-draggable').css('left',leftOffset);

    if(debugFlag){
      updateDebugTargetPos();
    }

    clearTimeout(autoPlayTimeoutInterval);
    if(autoPlayScrollTL != null ) {
        autoPlayScrollTL.kill();
        beginningAnimationFlag = false;
        autoPlayingFlag = false;
    }

    if(timelinePercentage <= 0.88 && !finalBubbleContainer.visible) {
      finalBubbleContainer.visible = true;
      exploreBubbleContainer.visible = false;
    }

    $('.tl-grabber-draggable').on('mouseup touchend', function() {
      $(this).removeClass('tl-grabber-draggable');
    });
  }

  function updateTimeGrabberPosition(animatedFlag) {
    var draggerWidth= $tlGrabber.outerWidth();
    var timelineWidth = $('.scrubber-box').width()-draggerWidth;
    var leftOffset = timelineWidth * timelinePercentage;
    if(animatedFlag) {
      TweenMax.to($tlGrabber,0.5,{left:leftOffset+"px"});
    } else {
      $tlGrabber.css('left',leftOffset);
    }
  }

  function calculateMoveContainerMaxMinPos() {
    moveContainerMaxPos = -section6Container.position.x - 1520 + (($tlContainer.width()/2)/scaleContainer.scale.x);
    moveContainerMinPos = (($tlContainer.width()/2)/scaleContainer.scale.x) - 382;

  }

  function setMoveContainerStartPos() {
    moveContainerCurrentPos.x = moveContainerMaxPos;
    moveContainer.position = moveContainerCurrentPos;
    var draggerWidth= $tlGrabber.outerWidth();
    var draggerCenter = $('.tl-grabber').outerWidth()/2;
    var timelineWidth = $('.scrubber-box').width()-draggerWidth;
    var leftOffset = 0;
    leftOffset = timelineWidth * timelinePercentage;
    $tlGrabber.css('left',leftOffset);
  }

  function gotoBeginning(autoFlag) {
    // calculateMoveContainerMaxMinPos();
    beginningAnimationFlag = true;
    moveContainerPositionUpdateFlag = false;
    autoPlayingFlag = true;
    var percentCount = {val:1};
    TweenMax.to(exploreBubbleContainer,0.5,{alpha:0,onComplete:function(e) {
      this.target.visible = false;
    }});
    TweenMax.to('.timeline-grabber',4,{'left':0,ease:Power4.easeInOut});
    TweenMax.to(percentCount,3,{val:0,ease:Linear.easeNone,onUpdate:function() {
      timelinePercentage = percentCount.val;
      updateScrubberTimeText();
    }, onComplete: function() {

     clearTimeout(autoPlayTimeoutInterval);

      finalBubbleContainer.visible = true;
    }})

    TweenMax.to(moveContainer,4,{x:moveContainerMinPos,ease:Power4.easeInOut, onComplete:function() {

      moveContainerCurrentPos.x = moveContainerMinPos;
      moveContainer.position.x = moveContainerMinPos;
      moveContainerTarget = moveContainerMinPos;

      beginningAnimationFlag = false;
      animationCheck(true);
      startAutoPlay();
    }});
  }

  function setupAutoPlay() {
    var totalTime = 200;
    var timelineWidth = $('.scrubber-box').width();
    var draggerCenter = $('.timeline-grabber').outerWidth();

    var leftOffset = timelineWidth-draggerCenter;

    var percentCount = {val:0};
    var scrollEasing = Power0.easeNone;
    autoPlayScrollTL = new TimelineMax({paused:true,delay:3, onUpdate:function() {
      animationCheck();
    }});
    autoPlayScrollTL.add([
      TweenMax.to(percentCount,totalTime,{val:1, ease: scrollEasing,
        onUpdate:function(){
          timelinePercentage = percentCount.val;

          updateScrubberTimeText();
          animationCheck();
        }
      }),

      TweenMax.to(moveContainerCurrentPos, totalTime,{x:moveContainerMaxPos, ease: scrollEasing,
        onUpdate:function() {
          moveContainerTarget = moveContainerCurrentPos;
          moveContainer.position = moveContainerCurrentPos;
        }
      }),

      TweenMax.to('.timeline-grabber',totalTime,{'left':leftOffset,ease:Power4.easeInOut, ease: scrollEasing})
    ]);
  }

  function startAutoPlay() {
    setupAutoPlay();
    if(autoPlayScrollTL != null) {
      // console.log('autoplayingflag:'+autoPlayingFlag);
      autoPlayScrollTL.play();
    }
    beginningAnimationFlag = false;
  }

  function createPopupBubble(iconTexture, textTexture, x, y, parent, cornerX, cornerY, xOpenOffset, yOpenOffset) {
    var scaling = 0.9;
    if(isMobile) {
      scaling = 1.3;
    }
    var tl = new TimelineMax({paused: true});
    tl.timeScale(2);
    var iconBox = new PIXI.Container();
    var hitArea = new PIXI.Graphics();
    hitArea.beginFill(0x000000,0);
    hitArea.drawRect(-27.5,-27.5,55,55);
    hitArea.endFill();
    var icon = new PIXI.Sprite(iconTexture);
    icon.anchor = new PIXI.Point(0.5,0.5);

    iconBox.addChild(icon);
    iconBox.addChild(hitArea);

    iconBox.scale.set(scaling);
    iconBox.buttonMode = true;
    iconBox.interactive = true;

    var iconBoxRect = iconBox.getBounds();

    var box = new PIXI.Sprite(textTexture);
    box.anchor = new PIXI.Point(0.5,0.5);
    box.scale.set(0);
    box.alpha = 0;

    var container = new PIXI.Container();
    container.position = new PIXI.Point(x,y);

    container.addChild(box);
    container.addChild(iconBox);

    if(!xOpenOffset) {
      var xOpenOffset = 0;
    }
    if(!yOpenOffset) {
      var yOpenOffset = 0;
    }

    var iconBoxCornerX = cornerX * scaling;
    var iconBoxCornerY = cornerY * scaling;

    var iconBoxCornerXDiff = iconBoxCornerX - cornerX;
    var iconBoxCornerYDiff = iconBoxCornerY - cornerY;

    xOpenOffset -= iconBoxCornerXDiff;
    yOpenOffset -= iconBoxCornerYDiff;

    // xOpenOffset = xOpenOffset + (*scaling);
    // yOpenOffset = yOpenOffset + (*scaling);


    tl.add([
      TweenMax.to(container,0.5,{y:centerHeight+yOpenOffset-(iconBoxRect.height*0.5),x:(container.position.x + xOpenOffset)}),
      TweenMax.to(iconBox,0.5,{x:iconBoxCornerX, y:iconBoxCornerY}),
      TweenMax.to(box.scale,0.5,{x:scaling,y:scaling}),
      TweenMax.to(box,0.5,{alpha:1})
    ]);

    if(isMobile) {
      iconBox.touchend = function(e) {
        openthisIcon(e);
      }
    } else {
      iconBox.mouseup = function(e) {
        openthisIcon(e);
      }
    }

    var openthisIcon = function(e) {
      clearTimeout(autoPlayTimeoutInterval);
      if(autoPlayScrollTL != null ) {
          autoPlayScrollTL.kill();
          beginningAnimationFlag = false;
          autoPlayingFlag = false;
      }
      closeAllBubbles();
      if(tl.progress() == 0) {
        tl.play();
      }
    }

    parent.addChild(container);

    createAnimationCheckPoint(tl,x,100, 0,0xffffff,true);

    return tl;
  }

  function draw() {
    window.requestAnimationFrame(draw.bind(this)); // start the timer for the next animation loop

    if(moveContainerPositionUpdateFlag) {
      moveContainerDist = moveContainerTarget - moveContainerCurrentPos.x;
      var moveCheck = Math.abs( Math.round(moveContainerDist*100)/100 );
      if(moveCheck >= 0.01) {
        moveContainerCurrentPos.x += moveContainerDist*0.1;
        moveContainer.position = moveContainerCurrentPos;
        if(!beginningAnimationFlag)
          animationCheck();
      } else {
        moveContainerPositionUpdateFlag = false;
      }
    }
    renderer.render(stage);

  }

  function updateScrollTarget() {
    moveContainerTarget = ((moveContainerMaxPos - moveContainerMinPos) * timelinePercentage) + moveContainerMinPos;
    moveContainerPositionUpdateFlag = true;
  }

  function updateScrubberTimeText() {

    // var time = Math.floor(timeDiff * timelinePercentage) + timeTextMin;
    // console.log(timelinePercentage);

    var i = 0
    var yearsLen = yearTimes.length-2;
    var thisYear;
    var nextYear;
    for(i; i <= yearsLen; i++) {
      // console.log(i);

      thisYear = yearTimes[i];
      nextYear = yearTimes[i+1];
      if(timelinePercentage >= thisYear.perc && timelinePercentage < nextYear.perc) {
        $scrubberTimeText.text(thisYear.year);
        // console.log(thisYear.year);

      }
    }
  }

  function animationCheck(skipDebounceFlag) {
    if(!skipDebounceFlag) {
      animationCheckDeBounceCount++;

      if(animationCheckDeBounceCount <= animationCheckDeBounceCountLimit)
        return;
      animationCheckDeBounceCount = 0;
    }

      var i = animationCheckPoints.length-1;
      var thisAnim;
      var position = -moveContainer.position.x;

      for(i; i >= 0; i--) {
        thisAnim = animationCheckPoints[i];
        if(thisAnim.isReset) {
          if(position >= (thisAnim.leftSide-viewXCenter) && position <= (thisAnim.rightSide-viewXCenter)) {
            if(thisAnim.isPopup) {
              if(!autoPlayingFlag)
                return;
              closeAllBubbles();
            }
            thisAnim.tl.play();
            thisAnim.isReset = false;
          }
        }
        else if(!thisAnim.isReset && !thisAnim.isPopup) {
          if(position <= (thisAnim.leftSideReset-viewXCenter) || position >= (thisAnim.rightSideReset-viewXCenter)) {
            thisAnim.tl.gotoAndStop(0);
            thisAnim.isReset = true;
          }
        }
      }
  }

  function closeAllBubbles() {
    // var thisBubbleIndex = bubbleTimelineArray.indexOf(tlBubble);
    var thisAnim;
    var i = animationCheckPoints.length-1;
    for(i; i >= 0; i--) {
      thisAnim = animationCheckPoints[i];
      if(!thisAnim.isPopup)
        return;
      thisAnim.tl.reverse();
      thisAnim.isReset = true;
    }
  }

  function createAnimationCheckPoint(tl,posX,activeRange, resetRange,debugColor,isPopup) {
    animationCheckPoints.push({
      tl:tl,
      posX:posX,
      activeRange:activeRange,
      resetRange:resetRange,
      leftSide:posX-(activeRange/2),
      rightSide:posX+(activeRange/2),
      leftSideReset: posX-(resetRange/2),
      rightSideReset: posX+(resetRange/2),
      isReset:true,
      debugColor: debugColor,
      isPopup: isPopup
    });
  }

  function scrollDownToNextSection() {
    // Handle smooth scrolling
    var $selector = $('#message-board-chair'),
      position = Math.round($selector.offset().top);


      if ($window.scrollTop() + topOffset !== position) {
        $htmlBody.stop();
        $htmlBody.animate({scrollTop: position - topOffset}, '1000', 'linear', function () {
          return false;
        });
      }
  }

  function addDebugView() {
    debugViewContainer = new PIXI.Container();
    // debugViewContainer.width = $tlContainer.width();
    // debugViewContainer.height = $tlContainer.height();

    debugleftBorder = new PIXI.Container();
    debugrightBorder = new PIXI.Container();
    debugcenterBorder = new PIXI.Container();

    var leftBorderG = new PIXI.Graphics();
    var rightBorderG = new PIXI.Graphics();
    var centerBorderG = new PIXI.Graphics();
    debugActivationRegion = new PIXI.Graphics();

    leftBorderG.beginFill(0x598b7d,0.9);
    leftBorderG.drawRect(0,0,5,500);
    leftBorderG.endFill();
    debugleftBorder.addChild(leftBorderG);

    rightBorderG.beginFill(0xD35E29,0.9);
    rightBorderG.drawRect(-5,0,5,500);
    rightBorderG.endFill();
    debugrightBorder.addChild(rightBorderG);

    centerBorderG.beginFill(0x383B70,0.9);
    centerBorderG.drawRect(-2.5,0,5,500);
    centerBorderG.endFill();
    debugcenterBorder.addChild(centerBorderG);


    debugrightBorder.position = new PIXI.Point($tlContainer.width()/scaleContainer.scale.x,0);
    debugcenterBorder.position = new PIXI.Point(($tlContainer.width()/2)/scaleContainer.scale.x,0);

    debugViewContainer.addChild(debugleftBorder);
    debugViewContainer.addChild(debugrightBorder);
    debugViewContainer.addChild(debugcenterBorder);

    debugTargetXDisplay = new PIXI.Text(timelinePercentage,{fill:0x000000,align:'left'});
    debugTargetXDisplay.position = new PIXI.Point(0,30);
    debugViewContainer.addChild(debugTargetXDisplay);

    scaleContainer.addChild(debugViewContainer);
    populateVisualActivationDegugRegions();
  }

  function updateDebug() {
    debugrightBorder.position = new PIXI.Point($tlContainer.width(),0);
    debugrightBorder.position = new PIXI.Point($tlContainer.width()/scaleContainer.scale.x,0);
    debugcenterBorder.position = new PIXI.Point(($tlContainer.width()/2)/scaleContainer.scale.x,0);
  }

  function populateVisualActivationDegugRegions() {
    var i = animationCheckPoints.length-1;
    var thisAnim;
    for(i; i >= 0; i--) {
      createSingleVisualActivationDebugRegion(animationCheckPoints[i]);
    }
  }

  function createSingleVisualActivationDebugRegion(obj) {
    var width = obj.activeRange;
    var resetWidth = obj.resetRange;
    var posX = obj.posX;
    var height = 500;
    var color = obj.debugColor;

    var stick = new PIXI.Graphics();
    stick.beginFill(color, 0.9);
    stick.drawRect(-3,0,6,height);
    stick.endFill();
    stick.position = new PIXI.Point(posX,0);

    var region = new PIXI.Graphics();
    region.beginFill(color, 0.5);
    region.drawRect(-width/2,0,width,height);
    region.endFill();
    region.position = new PIXI.Point(posX,0);

    var resetRegion = new PIXI.Graphics();
    resetRegion.beginFill(color, 0.2);
    resetRegion.drawRect(-resetWidth/2,0,resetWidth,height);
    resetRegion.endFill();
    resetRegion.position = new PIXI.Point(posX,0);

    moveContainer.addChild(resetRegion);
    moveContainer.addChild(region);
    moveContainer.addChild(stick);
  }

  function updateDebugTargetPos() {
    // scaleContainer.scale.x
    var actualTarget = moveContainerTarget - debugcenterBorder.position.x;
    // var actualTarget = moveContainer.getBounds().width;
    debugTargetXDisplay.text = actualTarget;
    // debugTargetXDisplay.text = timelinePercentage;
  }

  function updateViewXCenter() {
    viewXCenter = ($tlContainer.width()/2)/scaleContainer.scale.x;
  }

  /**
   * Uninitialize this module.
   */
  function deinit() {
    isInitialized = true;
  }


  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    },
    isInitialized: function() {
      return isInitialized;
    }
  };
})(jQuery);
