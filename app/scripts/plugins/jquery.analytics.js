'use strict';
(function ($) {
  var gaObject = {};

  gaObject.base = {
    page: function (url) {
      if (typeof(ga) !== "undefined") {
        this._pageViewUniversal(url);
      } else if (typeof(_gaq) !== "undefined") {
        this._pageViewClassic(url);
      }
    },
    event: function (category, action, label) {
      if (typeof(ga) !== "undefined") {
        this._eventTrackUniversal(category, action, label);
      } else if (typeof(_gaq) !== "undefined") {
        this._eventTrackClassic(category, action, label);
      }
    },
    event_link: function (category, action, label, url, win) {
      if (typeof(ga) !== "undefined") {
        this._eventTrackCallbackUniversal(category, action, label, url, win);
      } else if (typeof(_gaq) !== "undefined") {
        this._eventTrackCallbackClassic(category, action, label, url, win);
      }
    },
    init: function (callback, options) {
      this._options = options;
      this._debug = (window.location.hostname.indexOf('shomimrrobot.com') >= 0) ? false : true;
    },

    _options: {},
    _eventTrackClassic: function (cat, action, label) {
      if (typeof(_gaq) !== "undefined") {
        if (this._debug) {
          console.log('eventTrack: - ' + 'category: ' + cat + ',action: ' + action + ', label: ' + label);
        } else {
          _gaq.push(['_trackEvent', cat, action, label]);
        }
      }
    },
    _eventTrackCallbackClassic: function (cat, action, label, url, win) {
      if (typeof(_gaq) !== "undefined") {
        if (this._debug) {
          console.log('eventTrack: - ' + 'category: ' + cat + ',action: ' + action + ', label: ' + label);
        } else {
          _gaq.push(['_trackEvent', cat, action, label]);
        }

        setTimeout(function () {
          if (win && win.length > 0) {
            window.open(url, win);
          } else {
            window.location.href = url;
          }
        }, 300);
      }
    },
    _pageViewClassic: function (gotoURL) {
      var sPath = window.location.pathname;
      var sPage = (typeof gotoURL != 'undefined') ? gotoURL : sPath.substring(sPath.lastIndexOf('/') + 1);
      if (typeof(_gaq) !== "undefined") {
        if (this._debug) {
          console.log('pageView: - ' + 'page: ' + sPage);
        } else {
          _gaq.push(['_trackPageview', sPage]);
        }
      }
    },
    _eventTrackUniversal: function (category, action, label) {
      if (typeof(ga) !== "undefined") {
        ga('send', 'event', category, action, label);
      }
    },
    _eventTrackCallbackUniversal: function (category, action, label, url, win) {
      if (typeof(ga) !== "undefined") {
        ga('send', 'event', category, action, label,
          {'hitCallback': this._hitCallbackHandler(url, win)}
        );
      }
    },
    _hitCallbackHandler: function (url, win) {
      if (win !== 'undefined' || win || win.length > 0) {
        window.open(url, win);
      } else {
        window.location.href = url;
      }
    },
    _pageViewUniversal: function (gotoURL) {
      var sPath = window.location.pathname;
      var sPage = (typeof gotoURL != 'undefined') ? gotoURL : sPath.substring(sPath.lastIndexOf('/') + 1);

      if (typeof(ga) !== "undefined") {
        ga('send', 'pageview', sPage);
      }
    }
  };
  var self = $.extend({}, gaObject.base);
  self.init();
  $.ga = self;
})(jQuery);
