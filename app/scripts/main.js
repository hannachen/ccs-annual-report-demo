'use strict';
window.onload = function() {

  /*global TweenLite, enquire, timeline, graphs, youtube, analytics, utils*/

  // Modules
  timeline.init();
  graphs.init();
  youtube.init();
  analytics.init();

  const $window = $(window),
        $htmlBody = $('html, body'),
        $body = $('body'),
        $mainMenu = $('#main-menu'),
        $overlayBg = $('<div class="overlayBg"></div>'),
        graphElements = utils.setAsArray($('.graph')),
        mobileOffset = 65,
        desktopOffset = 85;

  // Set up global variables
  var isScrolling = false,
      isMobile = window.matchMedia('screen and (max-width: 48em)').matches,
      topOffset = isMobile ? mobileOffset : desktopOffset,
      timeoutId = null;

  // Handle breakpoints using JS
  enquire.register('screen and (max-width: 48em)', {
    match: function() {
      isMobile = true;
      topOffset = mobileOffset;
      graphs.setupGraphs();
    },
    unmatch: function() {
      isMobile = false;
      topOffset = desktopOffset;
      graphs.setupGraphs();
    }
  });

  // Main navigation -- Scrollspy
  $body.scrollspy({
    target: '#main-menu',
    offset: topOffset + 50
  });

  // Nav toggle callbacks
  $mainMenu.on('hidden.bs.collapse', function () {
    $overlayBg.off('click').remove();
  });

  $mainMenu.on('shown.bs.collapse', function() {
    $overlayBg.on('click', function() {
      $mainMenu.collapse('hide');
    });
    $body.prepend($overlayBg);
  });

  // Disable empty anchor links
  $('a[href="#"]').on('click', function(e) {
    e.preventDefault();
  });

  // Handle smooth scrolling
  $('.nav-link').on('click', function(e) {
    e.preventDefault();
    var $currentTarget = $(e.currentTarget),
        href = $currentTarget.attr('href'),
        $selector = $(href),
        position = Math.round($selector.offset().top);

    // Close nav on nav item click
    $mainMenu.collapse('hide');

    // Jump to position
    if ($window.scrollTop() + topOffset !== position) {
      $htmlBody.stop();
      $htmlBody.animate({scrollTop: position - topOffset}, '1500', 'easeInOutExpo', function () {
        return false;
      });
    }

    // Highlight items in rows, if exists
    $selector.find('.link-element').focus();
  });

  // Glossary section
  $('.term').on('click', function(e) {
    e.preventDefault();
    var $currentTarget = $(this);
    $currentTarget.next('.definition').toggle();
    $currentTarget.parent().toggleClass('active');
  });

  //Download previous annual reports section
  $('.prev-report-button-expand').on('click',function(e) {
    e.preventDefault();
    var $currentTarget = $(this),
    href = $currentTarget.attr('href'),
    $selector = $(href),
    position = Math.round($selector.offset().top);
    $currentTarget.toggleClass('active');
    $('.pre-reports-footer').collapse('toggle');
    $htmlBody.animate({scrollTop: position + 325}, '1500', 'easeInOutExpo', function () {
      return false;
    });
  });

  // Handle graphs
  elementsInView(graphElements, $window);

  // Attach window resize to graphs
  $window.on('resize', function() {
    if (timeoutId) {
      window.clearTimeout(timeoutId);
      timeoutId = window.setTimeout(function () {
        graphs.setupGraphs();
      }, 200);
    }
  });

  // Handle scroll events
  $window.on('scroll', function() {
    if(!isScrolling) {
      window.requestAnimationFrame(function onRequestAnimationFrame() {
        elementsInView(graphElements, $window);
        isScrolling = false;
      });
      isScrolling = true;
    }
  });

  /**
   * Check if graph elements are in view
   */
  function elementsInView(elements, $window) {

    // Set up plugin to detect whether an element is visible in the viewport
    for (let i=0; i < elements.length; i++) {
      let $el = elements[i],
          inView = utils.isElementsInView($el, $window);

      // Update element state
      setInView($el, inView);

      // Handle IE (does not support SVG transform), fall back using GASP
      if (utils.isIe) {
        $el.trigger({
          type: 'inview',
          inView: inView
        });
      }
    }
  }

  /**
   * Add or remove 'in' class based on the element's in view status.
   * @param {jQuery} $el - Element to check for and apply class to
   * @param {boolean} inView - Element's in view status
   */
  function setInView($el, inView) {
    if (inView) {
      $el.addClass('in');
    } else {
      $el.removeClass('in');
    }
  }

};

// Disable pull to refresh
var maybePreventPullToRefresh = false,
    lastTouchY = 0;

// Disable pull to refresh
var touchstartHandler = function(e) {
  if (e.touches.length !== 1) {
    return;
  }
  lastTouchY = e.touches[0].clientY;
  // Pull-to-refresh will only trigger if the scroll begins when the
  // document's Y offset is zero.
  maybePreventPullToRefresh = window.pageYOffset === 0;
};

var touchmoveHandler = function(e) {
  var touchY = e.touches[0].clientY,
    touchYDelta = touchY - lastTouchY;

  lastTouchY = touchY;

  if (maybePreventPullToRefresh) {
    // To suppress pull-to-refresh it is sufficient to preventDefault the
    // first overscrolling touchmove.
    maybePreventPullToRefresh = false;
    if (touchYDelta > 0) {
      e.preventDefault();
      return;
    }
  }
  if (window.pageYOffset === 0 && touchYDelta > 0) {
    e.preventDefault();
    return;
  }
};

document.addEventListener('touchstart', touchstartHandler, false);
document.addEventListener('touchmove', touchmoveHandler, false);
