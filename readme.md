# Coast Capital Savings 2015 Annual Report

## About this project
- This project is mobile first, when defining CSS rules, default applies to the smallest breakpoint.
- Generated with yeoman generator-webapp v2.0.0 with SASS, Bootstrap (v4.0.0-alpha.2 pre-release), and Modernizr

---

## Getting Started
- `npm install`
- `gulp install`
- `gulp serve`

---

## Links

#### Dev
http://ccsar2015.rethinkcanada.ca

#### Production
http://annualreport.coastcapitalsavings.com

---

## Deployment

Run `gulp build` before deploying to the dev server.

### Deploy to Rethink dev server
http://ccsar2015.rethinkcanada.ca `rethink/~m4cb00k`  
Dev environment is hosted on Rethink's Dreamhost servers.

Run `gulp deploy` to deploy files in **/dist** to the dev server.

### Deploy to production server
http://annualreport.coastcapitalsavings.com
Production site hosted on Rethink3, under client's subdomain.

Run `gulp deploy:prod` to deploy files in **/dist** to the prod server.

**Troubleshooting Notes**  
If deployed files cannot be viewed due to permission issues, please use FTP to set permissions as a temporary workaround.

